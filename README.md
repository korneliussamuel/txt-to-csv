<b>TXT => CSV file converter</b>

HOW TO USE
- copy binary file (`txt-to-csv`) to your local machine
- if the file does not have executable permission, run `chmod +x txt-to-csv`
- to execute: `./txt-to-csv -i {{path_to_input_file}} -o {{path_output_file}} -t {{remove_top_rows}} -b {{remove_bottom_rows}}`
    - example: `./txt-to-csv -i input.txt -o output.csv -t 5 -b 2`
    - the app will convert a file name input.txt (within the same directory) to output.csv
    - in the conversion, the app will ignore 5 top rows and the last 2 bottom rows from input file

FLAGS:
- `-i` : path_to_input_file, default value; *input.txt*
- `-o` : path_to_output_file, default value: *output.txt*
- `-t` : remove_top_rows, default value: *0*
- `-b` : remove_bottom_rows, default value: *0*